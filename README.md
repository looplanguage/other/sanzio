# Sanzio

---

This is the main source code repository for [Sanzio](https://looplang.org/docs/internal/sanzio) the LuaJIT backend for [Arc](https://looplang.org/docs/internal/arc).

## Get Started
Building a release target is simple and requires just executing one command:

`$ ./build.sh prod`

## Development
Building a development target is a little more involved. This is due to that Sanzio will look in the parent directory 
for [Vinci](https://gitlab.com/looplanguage/vinci) instead of pulling it from git.

First you need a central location to store your repositories and enter it.

```sh
$ mkdir repositories
$ cd repositories
```

Now you need to clone [Vinci](https://gitlab.com/looplanguage/vinci) and this repository

```sh
$ git clone https://gitlab.com/looplanguage/vinci
$ git clone https://gitlab.com/looplanguage/sanzio
```

Finally enter the `sanzio` and use the build script to build a development build.

```sh
$ cd sanzio
$ ./build.sh dev
```

## Running Tests
Make sure you followed the steps for the development guide above up until running `./build.sh dev`.

Running the tests is simple and uses the development target, just run:
```sh
$ ./build.sh citest
```

If you want to test with the git upstream version of Vinci instead of using the development one, run this instead.
```sh
$ ./build.sh test
```