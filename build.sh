#!/bin/bash
if ! command -v cargo &> /dev/null
then
    echo "Cargo could not be found!"
    exit 1
fi

move_to_prod() {
  mv Cargo.toml Cargo-production.toml
  mv Cargo-dev.toml Cargo.toml
}

move_to_dev() {
  mv Cargo.toml Cargo-dev.toml
  mv Cargo-production.toml Cargo.toml
}


if [ "$1" == "dev" ]
then
  echo "Building development build..."
  move_to_prod
  cargo build || move_to_dev
  move_to_dev
elif [ "$1" == "mvdev" ]
then
  echo "Moving to dev build..."
  move_to_prod
  echo "Done! Don't forget to move back to prod before pushing!"
elif [ "$1" == "mvprod" ]
then
  echo "Moving to production build..."
  move_to_dev
  echo "Done! You can safely push now!"
elif [ "$1" == "prod" ]
then
  echo "Building production build..."
  cargo build --release
elif [ "$1" == "cilint" ]
then
  echo "Linting CI..."
  cargo clippy -- -D warnings
  cargo fmt --all -- --check
elif [ "$1" == "ci" ]
then
  echo "Running CI tests..."
  cargo test
elif [ "$1" == "test" ]
then
  echo "Running tests..."
  move_to_prod
  cargo test || move_to_dev
  move_to_dev
else
  echo "Unknown command \"$1\", expected: dev, prod, test"
  exit 1
fi